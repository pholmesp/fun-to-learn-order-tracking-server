var express = require('express');
var router = express.Router();
var path = require('path');
var fs = require('fs');
var multer  = require('multer');
var dbServices = require('../moudle/db-service');
var passport = require('../config/passport');
var mail = require('../moudle/qqmail');
var dateFormat = require('dateformat');
var zipFiles = require('../moudle/zipFiles');

/* GET home page. */
router.get('/doc', function(req, res, next) {
  res.sendFile(path.join(__dirname, '../client/build/index.html'));
  // fs.createReadStream(path.join(__dirname, '../client/build/index.html')).pipe(res);
});

router.get('/log', function(req,res,next){
  console.log(__dirname)
  fs.createReadStream(path.join(__dirname, '../nohup.out'))
  .pipe(res);
});

router.post('/updateOrder', function(req,res){
  let key = req.body.key
  let value=req.body.value
  let idorder = req.body.idorder
  let fields = req.body.fields
  let section = []
  for(let field of fields){
    section.push(` ${field.key}='${field.value}' `)
  }
  section = section.join(',')
  let sql = `UPDATE orders SET ${section} WHERE idorder=${idorder}`
  console.log(sql)
  dbServices.query(sql, function(error, data){
    if(error) res.json(error)
    else{
      res.json(data)
    }
  })
})

router.post('/updatePackage', function(req,res){
  let key = req.body.key
  let value=req.body.value
  let idorderPackage = req.body.idorderPackage
  let fields = req.body.fields
  let section = []
  for(let field of fields){
    section.push(` ${field.key}='${field.value}' `)
  }
  section = section.join(',')
  let sql = `UPDATE orderpackage SET ${section} WHERE idorderPackage=${idorderPackage}`
  console.log(sql)
  dbServices.query(sql, function(error, data){
    if(error) res.json(error)
    else{
      res.json(data)
    }
  })
})

router.post('/login',
  passport.authenticate('user'),
  function(req, res) {
    console.log('login', req.user)
    res.json(req.user);
  })

  router.post('/searchbycourse',function(req, res) {
    //var a = JSON.parse(req.body.course)
    //console.log(req.body.course)
    var c = `%${req.body.course}%`
    //console.log(c)
    var sql = "SELECT a.idorder as idkey, a.idcourse, b.courseCode, b.name, a.name as orderName,\
     a.salePrice, a.currency_sale, c.score, c.scorePercent, c.scoreShot, a.fileUrl \
     FROM orders as a, cource as b, ordersubmit as c where a.idorder = c.orders_idorder \
     and a.idcourse = b.idcource and  a.status = 'complete' and courseCode like ?"
    dbServices.query(sql, c, function(error,dd){
        
      if(error){
        return res.json(error);
      }
      else{
        //console.log(JSON.parse(dd[0].scoreShot)[0].path)
        res.json(dd)
      }   
    })
    
  })

  router.post('/getAllSaleOrdersByWechat', function(req,res){
    let idUser = req.body.iduser
    
    // let section = []
    // for(let field of fields){
    //   section.push(` ${field.key}='${field.value}' `)
    // }
    // section = section.join(',')
    // let sql_GetWechatByIduser = `SELECT wechat from user where iduser = ${idUser}` 
    let sql_GetWechatByIduser = `SELECT id from SalesWechat where idUser = ${idUser}`
    let sql_GetOrdersByWechat = `SELECT * from orders where wechat in (?) order by idorder desc`
    //console.log(sql)
    dbServices.query(sql_GetWechatByIduser, function(error, data){
      if(error) res.json(error)
//       else if (data[0].wechat !== null && data[0].wechat !== undefined){
//         console.log(data[0].wechat)
//         let wechats = []
//         for (let i =0; i<data[0].wechat.split('|').length;i++){
//           wechats.push(data[0].wechat.split('|')[i])
//         }
//         wechats.join(',')
//         //console.log(wechats)
// =======
      else {
        let wechats = data.map(ele=>ele.id)
        dbServices.query(sql_GetOrdersByWechat, [wechats],function(err, dd){
          if(err) res.json(err)
          else{
            res.json(dd)
          }
        })
      }
    })
  })
  router.post('/getAllSaleMonthByWechat', function(req,res){
    let idUser = req.body.iduser
    
    // let section = []
    // for(let field of fields){
    //   section.push(` ${field.key}='${field.value}' `)
    // }
    // section = section.join(',')
    //let sql_GetWechatByIduser = `SELECT wechat from user where iduser = ${idUser}`
    let sql_GetOrdersByWechatMonth = `select  DATE_FORMAT(a.dateTime, '%Y-%m')  as months, count(*) count \
    from ordertracking.orders as a, ordertracking.SalesWechat as b where a.status='complete' and a.orderPackage_idorderPackage is NULL 
    and a.wechat = b.id and b.idUser = ${idUser} group by DATE_FORMAT(dateTime, '%Y-%m') order by  DATE_FORMAT(dateTime, '%Y-%m') desc`
    //console.log(sql)
    
        dbServices.query(sql_GetOrdersByWechatMonth,function(err, dd){
          if(err) res.json(err)
          else{
            
            //console.log(dd)
            //console.log("xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx")
            res.json(dd)
          }
        })
      
    
  })

  router.post('/getAllSaleHistoryOrdersByWechatMonth', function(req,res){
    let idUser = req.body.iduser
    let Month = req.body.month
    
    // let section = []
    // for(let field of fields){
    //   section.push(` ${field.key}='${field.value}' `)
    // }
    // section = section.join(',')
    //let sql_GetWechatByIduser = `SELECT wechat from user where iduser = ${idUser}`
    let sql_GetOrdersByWechatMonth = `select a.* from ordertracking.orders as a, ordertracking.SalesWechat as b where a.status='complete'
    and a.wechat = b.id and  b.idUser = ${idUser} and DATE_FORMAT(a.dateTime, '%Y-%m') = ? order by a.idorder desc`
    //console.log(sql)
   
        dbServices.query(sql_GetOrdersByWechatMonth, Month,function(err, dd){
          if(err) res.json(err)
          else{
            
            //console.log(dd)
            //console.log("xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx")
            res.json(dd)
          }
        })
    
  })

  router.post('/getSaleCustomersByWechat', function(req,res){
    let idUser = req.body.iduser
    
    // let section = []
    // for(let field of fields){
    //   section.push(` ${field.key}='${field.value}' `)
    // }
    // section = section.join(',')
    // let sql_GetWechatByIduser = `SELECT wechat from user where iduser = ${idUser}`
    let sql_GetWechatByIduser = `SELECT id from SalesWechat where idUser = ${idUser}`
    let sql_GetCustomersByWechat = `SELECT a.*, b.wechatName from ordertracking.customer as a, ordertracking.SalesWechat as b \
    where a.wechat = b.id and a.wechat in (?) order by a.idcustomer desc`
    //console.log(sql)
    dbServices.query(sql_GetWechatByIduser, function(error, data){
      if(error) res.json(error)
      else if (data.length>0){
        
        //console.log(data)
        let wechats = data.map(ele=>ele.id)
        //console.log(wechats)
        dbServices.query(sql_GetCustomersByWechat, [wechats],function(err, dd){
          if(err) res.json(err)
          else{
            //console.log(dd)
            res.json(dd)
          }
        })
      }
    })
  })


router.get('/currentUser', passport.authorize('user'),
  function(req, res) {
    console.log('currentUser', req.user)
    res.json(req.user);
  })
router.post('/editUser',
  function(req, res) {
    console.log(req.user, req.user_f)
    res.json(req.user);
  })
router.get('/logout', (req, res)=> {
  req.logOut();
  res.json({'logout':'success'})
  // res.redirect('/');
  console.log('logout');
  //res.json({success: true, msg: 'Successful signed out.'})
  
});

router.post('/neworder', passport.authenticate('jwtUser'),function(req, res) {
  var a =0;
  //var sql = 'insert into orders set ?'
  var day = dateFormat(new Date(), "isoDateTime");
  var sql = 'INSERT INTO orders (creatTime, totalPrice, tableID, status, type, comment) VALUES (?, ?, ?, ?, ?, ?)';
  //console.log(req.body.id)
  //console.log(req.body.creatTime)
  dbServices.query(sql, [day, req.body.totalPrice, req.body.tableID, 1, 1, req.body.comment], function(error,dd){
        
  if(error){
    return res.json(error);
  }
  else{
    var id  = dd.insertId;
    
    //res.json({success: true, msg: id});
    
    var v = req.body.items;
    var objs = [];
    for (let i =0;i<v.length;i++){
      var obj = [v[i].num, id, v[i].DishID, v[i].type, v[i].comment];
      // var obj ={};
      // obj.orderID = id;
      // obj.DishID = v[i].DishID;
      // obj.DishCount = v[i].num;
      objs.push(obj);
    }
    //console.log("297 " + objs);
    var sq = 'INSERT INTO Items (DishCount, orderID, DishID, type, comment) VALUES ?'
    
    dbServices.query(sq, [objs], function(error){    
      if(error){      
        return res.json(error); 
      } else {
        var sqll = 'update tables set ? where id = ?';
        dbServices.query(sqll, [{status: 2, currentOrderID: id},req.body.tableID], function(error){
                
          if(error){
            return res.json(error);
          }
          else {
            res.json({success: true, msg: 'Successfully created the new order'});
            
          }
        });
      }
    })
  }   
})
});
    
var createFolder = function(folder){
  try{
    fs.accessSync(folder);
  }catch(e){
    fs.mkdirSync(folder);
  }
};

var uploadFolder = 'data/img/';
createFolder(uploadFolder);
var storage = multer.diskStorage({
  destination: function (req, file, cb) {
    cb(null, uploadFolder);    // 保存的路径，备注：需要自己创建
    
  },
  filename: function (req, file, cb) {
      // 将保存文件名设置为 时间戳+原名，比如 1478521468943-logo
      var name = Date.now()+'-'+file.originalname;
      cb(null, name);
      
  }
});

// 通过 storage 选项来对 上传行为 进行定制化
 var uploadStorage = multer({ storage: storage })
//多图上传
router.post('/uploadFiles', uploadStorage.array('file', 100), function(req, res, next){
  console.log(req.body.fileclass)
  let files = []
  for(let file of req.files){
    file['fileclass'] = req.body.fileclass
    files.push(file)
  }
  // console.log(req.)
  // console.log(files)
  res.json(files)
});
// 单图上传
router.post('/uploadFile', uploadStorage.single('file'), function(req, res, next){
  console.log(req.file)
  res.json(req.file)
});

router.get('/img/:name', function(req, res, next){
  var form = fs.readFileSync(uploadFolder+req.params.name);
  res.send(form);
});
router.get('/assignment/:name', function(req, res, next){
  console.log(req.params.name)
  var assignment = fs.readFileSync('data/assignment/'+req.params.name);
  res.send(assignment);
});
router.get('/result/:name', function(req, res, next){
  console.log(req.params.name)
  var submit = fs.readFileSync('data/result/'+req.params.name);
  res.send(submit);
});


router.post('/mailXieshou', function(req, res){
  // console.log(req.body)
  if(!fs.existsSync('data/assignment/'+req.body.name+'.zip')){
    zipFiles(req.body.name, req.body.urls, req.body.textName, req.body.textContent, 'assignment')
  }
  var mailOptions = {
    from: "service@funtolearn.co",
    //req.body.email
    to: req.body.email,
    subject: req.body.name, // Subject line
    html: `<p>Hi ${req.body.userName},</p>
    <br/>
    <p>A new order <b>${req.body.name}</b> has been assigned to you.</p>
    <p>You can login the <a href="https://console.funtolearn.co">Fun to Learn system</a> using your given email&passsword to get more detail.
    And you can do more operations on it.</p>
    <br/>
    <p>Download the compressed assignment materials directly: <a href="https://console.funtolearn.co/api/assignment/${req.body.name}.zip">${req.body.name}</a>. 
    </p><p>The .zip file includes all materials customer required. </p>
    <p>Please contact Fun to Learn using wechat or replying this email if you have any question. </p>
    <p>If the download doesn't triggered, please try using Chrome browser and press ctlr+shift+R to refresh the opened page. </p>
    <br/>
    <p>Best Regard,</p>
    <p><a href="https://funtolearn.co">Fun to Learn Team</a></p>`
  };
  // console.log(mailOptions)
  mail.sendMail(mailOptions, function(err, data){
    console.log('mail error', err, 'data', data)
    res.json(data)
  })
})
router.post('/mailCustomer', function(req, res){
  if(!fs.existsSync('data/result/'+req.body.name+'.zip')){
    zipFiles(req.body.name, req.body.urls, req.body.textName, req.body.textContent, 'result')
  }
  var mailOptions = {
    from: "service@funtolearn.co",
    to: req.body.email,
    subject: req.body.name, // Subject line
    html: `<p>Hi ${req.body.userName},</p>
    <br/>
    <p>The result for order <b>${req.body.name}</b> has been submited to you.</p>
    <br/>
    <p>Please download the compressed result directly by clicking <a href="https://console.funtolearn.co/api/result/${req.body.name}.zip">${req.body.name}</a>. 
    </p><p>The .zip file includes all materials and assistant notification. </p>
    <br/>
    <p>If the download doesn't triggered, please try using Chrome browser and press ctlr+shift+R to refresh the opened page. </p>
    <p>Please contact Fun to Learn using wechat or replying this email if you have any question. </p>
    <br/>
    <p>Best Regard,</p>
    <p><a href="https://funtolearn.co">Fun to Learn Team</a></p>`
  };
  // console.log(mailOptions)
  mail.sendMail(mailOptions, function(err, data){
    console.log('mail error', err, 'data', data)
    res.json(data)
  })
})

router.post('/mailAdmin', function(req, res){
  // console.log(req.body)
  
  var mailOptions = {
    from: "service@funtolearn.co",
    //"admin@funtolearn.co"
    to: "admin@funtolearn.co",
    subject: `Counsellor: ${req.body.counsellorName} has accepted order: ${req.body.orderId}`, // Subject line
    html: `<p>Hi Administrator,</p>
    <br/>
    <p>Counsellor <b>${req.body.counsellorName}(id: ${req.body.counsellorID})</b> has accepted the order <b>${req.body.orderId}: ${req.body.orderName}</b>.</p>
    <p>You can login the <a href="https://console.funtolearn.co">Fun to Learn system</a> using your given email&passsword to get more detail.
    And you can do more operations on it.</p>
    <br/>
    <p>Please contact Fun to Learn using wechat or replying this email if you have any question. </p>
    <br/>
    <p>Best Regard,</p>
    <p><a href="https://funtolearn.co">Fun to Learn Team</a></p>`
  };
  // console.log(mailOptions)
  mail.sendMail(mailOptions, function(err, data){
    //console.log('mail error', err, 'data', data)
    if (err){
      res.json(500, err)
    }
    else {
      res.json(data)
    }
  })
})

router.post('/mailSale', function(req, res){

  
  if(req.body.orderId){
    var sq = `SELECT b.* FROM ordertracking.orders as a, ordertracking.user as b where a.user_iduser = b.iduser and a.idorder = ${req.body.orderId}`
    
    dbServices.query(sq, function(error, dd){    
      if(error){      
        return res.json(error); 
      } else if(dd.length>0){
        //dd[0].email
        var email = dd[0].email
        var saleName = dd[0].name
        if (req.body.mode && req.body.mode ===1){
          var mailOptions = {
           from: "service@funtolearn.co",
           to: email,
           subject: `Counsellor: ${req.body.counsellorName} has accepted order: ${req.body.orderId}`, // Subject line
           html: `<p>Hi ${saleName},</p>
           <br/>
           <p>Counsellor <b>${req.body.counsellorName}(id: ${req.body.counsellorID})</b> has accepted the order <b>${req.body.orderId}: ${req.body.orderName}</b>.</p>
           <p>You can login the <a href="https://console.funtolearn.co">Fun to Learn system</a> using your given email&passsword to get more detail.
           And you can do more operations on it.</p>
           <br/>
           <p>Please contact Fun to Learn using wechat or replying this email if you have any question. </p>
           <br/>
           <p>Best Regard,</p>
           <p><a href="https://funtolearn.co">Fun to Learn Team</a></p>`
         };

         mail.sendMail(mailOptions, function(err, data){
          //console.log('mail error', err, 'data', data)
          if (err){
            res.json(500, err)
          }
          else {
            res.json(data)
          }
        })
       }
       else if (req.body.mode && req.body.mode ===2){
        var sql = `SELECT * FROM ordertracking.orders where idorder = ${req.body.orderId}`
    
        dbServices.query(sql, function(error, dd){    
          if(error){      
            return res.json(error); 
          } else if(dd.length>0){
            orderName = dd[0].name
            
            var mailOptions = {
              from: "service@funtolearn.co",
              to: email,
              subject: orderName, // Subject line
              html: `<p>Hi ${saleName},</p>
              <br/>
              <p>The result for order <b>${req.body.orderId}:${orderName}</b> has been submited to QA.</p>
              <br/>
              <p>Please contact Fun to Learn using wechat or replying this email if you have any question. </p>
              <br/>
              <p>Best Regard,</p>
              <p><a href="https://funtolearn.co">Fun to Learn Team</a></p>`
            };
            mail.sendMail(mailOptions, function(err, data){
              //console.log('mail error', err, 'data', data)
              if (err){
                res.json(500, err)
              }
              else {
                res.json(data)
              }
            })
          }
        })
          
       }
            
      }
      
    })
  }
  
  
  
})
Mail = (rEmail, rName, orderId, orderName, mode, counsellor)=> {
  var mailOptions = {
    from: "service@funtolearn.co",
    to: rEmail,
    subject: '', // Subject line
    html: ''
  }
  if (mode === 'Confirm'){
    mailOptions.subject = `Confirm ${orderName}`
    mailOptions.html = `<p>Hi ${rName},</p>
    <br/>
    <p>QA has confirmed and completed order <b>${orderId}:${orderName}</b>.</p>
    <br/>
    <p>Please contact Fun to Learn using wechat or replying this email if you have any question. </p>
    <br/>
    <p>Best Regard,</p>
    <p><a href="https://funtolearn.co">Fun to Learn Team</a></p>`
    
  }
  else if (mode === 'Modify'){
    mailOptions.subject = `Modify ${orderName}`
    mailOptions.html = `<p>Hi ${rName},</p>
    <br/>
    <p>QA has sent order <b>${orderId}:${orderName}</b> back to counsellor <b>${counsellor}</b> for modification.</p>
    <br/>
    <p>Please contact Fun to Learn using wechat or replying this email if you have any question. </p>
    <br/>
    <p>Best Regard,</p>
    <p><a href="https://funtolearn.co">Fun to Learn Team</a></p>`
  }
  else if (mode === 'Reject'){
    
    mailOptions.subject = `Reject ${orderName}`
    mailOptions.html = `<p>Hi ${rName},</p>
    <br/>
    <p>QA has rejected order <b>${orderId}:${orderName}</b> from counsellor <b>${counsellor}</b> and that order is now available to be assigned to the new writer.</p>
    <br/>
    <p>Please contact Fun to Learn using wechat or replying this email if you have any question. </p>
    <br/>
    <p>Best Regard,</p>
    <p><a href="https://funtolearn.co">Fun to Learn Team</a></p>`

  console.log(mailOptions)
  }

   return mail.sendMail(mailOptions).then(info=>{
    
    return ({"statusCode": 200, "res": rEmail})
    
   }
       
    ).catch(err=> {
      
      return ({"statusCode": 500, "res": rEmail})
      
    })

}

async function fetchURLs(orderId, salesName, orderName, xieshouName, mode, salesEmail, xieshouEmail) {
  try {
    
    // Promise.all() lets us coalesce multiple promises into a single super-promise
    var data = await Promise.all(
      
      [
      
        Mail( salesEmail,salesName,orderId ,orderName,mode, xieshouName).then((response) =>{ 
          return response
        }),
        Mail("admin@funtolearn.co", 'Administrator', orderId,orderName,mode, xieshouName).then((response) =>{
          return response
        }),
        Mail(xieshouEmail,xieshouName, orderId,orderName,mode, xieshouName).then((response) =>{
          return response
        } )
      
      
    ]
  );

    
    return data

  } catch (error) {
    console.log(error);
  }
}


router.post('/QaMail', function(req, res){
  
  if (req.body.orderId){
    var sql_GetXieShou = `select a.* from ordertracking.user as a, ordertracking.ordersubmit as b 
    where b.assigned_iduser = a.iduser and confirm = 1 and b.orders_idorder = ${req.body.orderId}`
    var sql_GetSales_Order = `select a.*, b.name as orderName from ordertracking.user as a, ordertracking.orders as b, ordertracking.SalesWechat as c
     where b.wechat = c.id and c.idUser = a.iduser and b.idorder = ${req.body.orderId}`

    dbServices.query(sql_GetXieShou, function(error, xieshou){    
      if(error){ 
        return res.json(error); 
      } else if(xieshou.length>0){
        dbServices.query(sql_GetSales_Order, function(error, sales){    
          if(error){ 
            return res.json(error); 
          } else if(sales.length>0){

            if (req.body.mode ==='Confirm'){
              //Email Sales sales[0].email
              var result = fetchURLs(req.body.orderId,  sales[0].name, sales[0].orderName, xieshou[0].name, req.body.mode,  sales[0].email, xieshou[0].email)
              result.then(val=>{
                res.json(val)
              })
                
                
                //Email Admin  admin@funtolearn.co
                
                
                //Email counsellor  xieshou[0].email
                
            

              // if (salesMailCon.statusCode ===200 && adminMailCon.statusCode ===200 && xieshouMailCon.statusCode ===200){
              //   res.json({'statusCode': 200})
              // }
              // else {
              //   var errorInfo = [salesMailCon, adminMailCon, xieshouMailCon]
              //   for (let i =0;i<3;i++){
              //     if (errorInfo[i].statusCode === 200){
              //       errorInfo.splice(i,1)
              //     }                 
              //   }
              //   res.json({'statusCode': 500, "res": errorInfo})
              // }
            }
            else if (req.body.mode ==='Modify'){
              //Email Sales sales[0].email
              //var salesMailMod = Mail(sales[0].email, sales[0].name, req.body.orderId,  sales[0].orderName, req.body.mode, xieshou[0].name)
              //Email Admin
              //var adminMailMod = Mail("admin@funtolearn.co", 'Administrator', req.body.orderId,  sales[0].orderName, req.body.mode, xieshou[0].name)
              //Email counsellor  xieshou[0].email
              //var xieshouMailMod = Mail(xieshou[0].email,  xieshou[0].name, req.body.orderId,  sales[0].orderName, req.body.mode, xieshou[0].name)
              var result = fetchURLs(req.body.orderId,  sales[0].name, sales[0].orderName, xieshou[0].name, req.body.mode, sales[0].email, xieshou[0].email)
              result.then(val=>{
                res.json(val)
              })
            
            }
            else if (req.body.mode ==='Reject'){
              
              //Email Sales sales[0].email
              //var salesMailRej = Mail(sales[0].email, sales[0].name, req.body.orderId,  sales[0].orderName, req.body.mode, xieshou[0].name)
              //Email Admin
              //var adminMailRej = Mail("admin@funtolearn.co", 'Administrator', req.body.orderId,  sales[0].orderName, req.body.mode, xieshou[0].name)
              //Email counsellor  xieshou[0].email
              //var xieshouMailRej = Mail(xieshou[0].email,  xieshou[0].name, req.body.orderId,  sales[0].orderName, req.body.mode, xieshou[0].name)
              var result = fetchURLs(req.body.orderId,  sales[0].name, sales[0].orderName, xieshou[0].name, req.body.mode, sales[0].email, xieshou[0].email)
              result.then(val=>{
                res.json(val)
              })
              
            }
            
          }
        })
      }
    })
      
   }  
})


module.exports = router;
