var express = require('express');
var path = require('path');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var cors = require('cors')
var path = require('path')
require('dotenv').config()
var dbServices = require('./moudle/db-service');
var dateFormat = require('dateformat');
var api = require('./routes/api');
var qapi = require('./routes/qapi')
var users = require('./routes/users');

const WebSocket = require('ws');

var app = express();
var passport = require('passport'),
    session = require("express-session"),
    bodyParser = require("body-parser");

// view engine setup_Mingjing
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');

// uncomment after placing your favicon in /public
app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'order-tracking/build')));

app.use(bodyParser.urlencoded({ extended: true }));
app.use(session({ 
  secret: "anything", 
  resave: true,
  saveUninitialized: false
}));
app.use(passport.initialize());
app.use(passport.session());
app.use(bodyParser.json())   

// app.all('*', ensureSecure); // at top of routing calls
if(process.env.MODE==='dev'){
  app.use('/api', cors({allowedHeaders:"Origin, X-Requested-With, Content-Type, Authorization, Accept, Authorization"}), api);
  app.use('/qapi', cors({allowedHeaders:"Origin, X-Requested-With, Content-Type, Authorization, Accept, Authorization"}), qapi);
}else{
  app.use('/api', api);
  app.use('/qapi', qapi);
}

app.use('/users', users);
// catch 404 and forward to error handler
app.use(function(req, res, next) {
  var err = new Error('Not Found');
  err.status = 404;
  next(err);
});


// development error handler
// will print stacktrace
if (app.get('env') === 'development') {
  app.use(function(err, req, res, next) {
    res.status(err.status || 500);
    res.render('error', {
      message: err.message,
      error: err
    });
  });
}

// production error handler
// no stacktraces leaked to user
app.use(function(err, req, res, next) {
  res.status(err.status || 500);
  res.render('error', {
    message: err.message,
    error: {}
  });
});

messageInsert = (message, from_iduser, salesID, status, type, orderID, cb, xieshouID, QAID)=> {
  var time = JSON.stringify(dateFormat(new Date(), "isoDateTime"));
  var res = undefined;
  var sql = undefined;
  console.log("AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA")
  console.log(type)
  if (type ===1){
    sql = `INSERT INTO ordertracking.messagequeue (dateTime, message, from_iduser, to_iduser, status, type, orderID)
      VALUES (${time}, ${message}, ${from_iduser}, ${salesID}, ${status},  ${type}, ${orderID}), (${time}, ${message}, ${from_iduser}, 1, ${status},  ${type}, ${orderID});`
  }
  else if (type ===2){
    sql = `INSERT INTO ordertracking.messagequeue (dateTime, message, from_iduser, to_iduser, status, type, orderID)
    VALUES (${time}, ${message}, ${from_iduser}, 1, ${status},  ${type}, ${orderID})`

  }
  else if (type ===3){
    sql = `INSERT INTO ordertracking.messagequeue (dateTime, message, from_iduser, to_iduser, status, type, orderID)
      VALUES (${time}, ${message}, ${from_iduser}, ${salesID}, ${status},  ${type}, ${orderID}), (${time}, ${message}, ${from_iduser}, 1, ${status},  ${type}, ${orderID});`
  }
  else if (type ===4){
    sql = `INSERT INTO ordertracking.messagequeue (dateTime, message, from_iduser, to_iduser, status, type, orderID)
      VALUES (${time}, ${message}, ${from_iduser}, ${salesID}, ${status},  ${type}, ${orderID}), (${time}, ${message}, ${from_iduser}, 1, ${status},  ${type}, ${orderID}), (${time}, ${message}, ${from_iduser}, ${xieshouID}, ${status},  ${type}, ${orderID});`
  }
  else if (type ===5){
    sql = `INSERT INTO ordertracking.messagequeue (dateTime, message, from_iduser, to_iduser, status, type, orderID)
      VALUES (${time}, ${message}, ${from_iduser}, ${salesID}, ${status},  ${type}, ${orderID}), (${time}, ${message}, ${from_iduser}, 1, ${status},  ${type}, ${orderID}), (${time}, ${message}, ${from_iduser}, ${QAID}, ${status},  ${type}, ${orderID});`
  }
  //console.log("AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA")
  //console.log(sql)
      if (sql !==undefined){
        dbServices.query(sql, function(error, data){
          if(error){
            console.log(`Error: ${error}`)
            res= {"successful": false, "error": error}
          }
          else if(data.affectedRows>0){
            console.log("AAAaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa")
            console.log(`Data: ${data}`)
            res= {"successful": true, "return": data}
            //res= {"successful": true, "return": data, "orderID": orderID, "xieshouID": xieshouID, "QAID": QAID, "salesID": salesID}
            console.log(data)
          }
          else {
            res= {"successful": false, "error": "No data inserted"}
          }
          cb(res)
          
        })
      }
      
}
messageSelect = (orderID, iduser, cb)=> {
  
      let sql = `Select * from ordertracking.messagequeue where to_iduser = ${iduser} and orderID = ${orderID};`
      let res = undefined
      dbServices.query(sql, function(error, data){
        if(error){
          res= {"successful": false, "error": error}
        }
        else if(data.length>0){
          res= {"successful": true, "return": data}
        }
        else {
          res= {"successful": false,  "error": "No data selected"}
        }
          cb(res)
        
      })
}
messageUpdate = (status, messageID)=> {
  let sql = `Update ordertracking.messagequeue set status = ${status} where idmessageQueue in (${messageID});`
  dbServices.query(sql, function(error, data){
    if(error){
      console.log({"successful": false, "error": error}) 
    }
    else if(data.affectedRows>0){
      return {"successful": true, "return": data}
    }
    else return {"successful": false,  "error": "No data updated"}
    
  })
}

sendMessage = (orderID, userID, userWS) => {
  messageSelect(orderID, userID, (data)=>{
    //console.log(res)
    let dat = data.return
    //console.log(dat)
    userWS.send(JSON.stringify(dat))
     messageUpdate(1, dat[0].idmessageQueue)
  })
}

const wss = new WebSocket.Server({port: 3006});

var clientMap = new Map();
var messageCacheMap = new Map();
wss.on('connection', function connection(ws) {
  //console.log("this ws: "+JSON.stringify(ws)+ws)
  // client = JSON.stringify(ws)
  // clientMap.set()

  ws.on('close', function close() {
    console.log('disconnected');
    // clientMap.forEach((key,value)=>{
    //   console.log(key+" : "+value)
    // });
    clientMap.forEach((value, key) => {
      if(value === ws) clientMap.delete(key)
  });
  
  });
  ws.on('error', function(e) {
    console.log("error occured");
  });
  ws.on('message', function incoming(message) {
    //console.log("AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA")
    //console.log(message)  
    var message = JSON.parse(message)
    if (message.type=='close'){
      clientMap.forEach((value, key) => {
        if(value === ws) clientMap.delete(key)
    });
    }
    else if(message.type=='conn'){
      console.log(message.idUser + 'log on...')
      clientMap.set(message.idUser, ws)
      //${message.idUser}
      let sql = `SELECT * FROM ordertracking.messagequeue where to_iduser = ${message.idUser} order by status asc, idmessageQueue desc`
      dbServices.query(sql, function(error, data){
        if(error){
          var err = {"type":"error", "content": error}
          ws.send(JSON.stringify(error))
        }
        else if(data.length>0){
          var ids = []
          for (let ele of data){
            ids.push(ele.idmessageQueue)
          }
          var Messages = JSON.stringify(data)
          ws.send(Messages)
          messageUpdate(1, ids)
        }
      })
    }
    else {
      //console.log("AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA")
      //console.log(message)
      if (message.type){
        var msg = message.message? message.message: JSON.stringify("")
        
        //to_iduser !!!!!!!!!!!!!!!!!!!!!!!!!
        //var friendWS = clientMap.get(message.from_iduser)
        //console.log("AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA")
        //console.log("BBBB "+friendWS)
        messageInsert(msg, message.from_iduser, message.to_salesID, 0, message.type, message.orderID, (res)=>{
          
            if(res.successful === true){
              if (message.type ===1){
                var salesWS = clientMap.get(message.to_salesID)
                var adminWS = clientMap.get(1)
                if(salesWS){
                  // messageSelect(message.orderID, message.to_salesID, (data)=>{
                  //   //console.log(res)
                  //   let dat = data.return
                  //   //console.log(dat)
                  //   salesWS.send(JSON.stringify(dat))
                  //    messageUpdate(1, dat[0].idmessageQueue)
                  // })
                  sendMessage(message.orderID, message.to_salesID, salesWS)
                }
                if(adminWS){
                  // messageSelect(message.orderID, 1, (data)=>{
                  //   //console.log(res)
                  //   let dat = data.return
                  //   //console.log(dat)
                  //   adminWS.send(JSON.stringify(dat))
                  //    messageUpdate(1, dat[0].idmessageQueue)
                  // })
                  sendMessage(message.orderID, 1, adminWS)
                }
                
              }
              else if (message.type ===2){
                var adminWS = clientMap.get(1)
                if(adminWS){
                  // messageSelect(message.orderID, 1, (data)=>{
                  //   //console.log(res)
                  //   let dat = data.return
                  //   //console.log(dat)
                  //   adminWS.send(JSON.stringify(dat))
                  //    messageUpdate(1, dat[0].idmessageQueue)
                  // })
                  sendMessage(message.orderID, 1, adminWS)
                }
              }
              else if (message.type ===3){
                var salesWS = clientMap.get(message.to_salesID)
                var adminWS = clientMap.get(1)
                if(salesWS){
                  // messageSelect(message.orderID, message.to_salesID, (data)=>{
                  //   //console.log(res)
                  //   let dat = data.return
                  //   //console.log(dat)
                  //   salesWS.send(JSON.stringify(dat))
                  //    messageUpdate(1, dat[0].idmessageQueue)
                  // })
                  sendMessage(message.orderID, message.to_salesID, salesWS)
                }
                if(adminWS){
                  // messageSelect(message.orderID, 1, (data)=>{
                  //   //console.log(res)
                  //   let dat = data.return
                  //   //console.log(dat)
                  //   adminWS.send(JSON.stringify(dat))
                  //    messageUpdate(1, dat[0].idmessageQueue)
                  // })
                  sendMessage(message.orderID, 1, adminWS)
                }
              }
              else if (message.type ===4){
                var salesWS = clientMap.get(message.to_salesID)
                var adminWS = clientMap.get(1)
                var xieshouWS = clientMap.get(message.to_xieshouID)
                if (salesWS){
                  sendMessage(message.orderID, message.to_salesID, salesWS)
                }
                if (adminWS){
                  sendMessage(message.orderID, 1, adminWS)
                }
                if (xieshouWS){
                  sendMessage(message.orderID, message.to_xieshouID, xieshouWS)
                }
              }
              else if (message.type ===5){
                var salesWS = clientMap.get(message.to_salesID)
                var adminWS = clientMap.get(1)
                var qaWS = clientMap.get(message.to_qaID)
                if (salesWS){
                  sendMessage(message.orderID, message.to_salesID, salesWS)
                }
                if (adminWS){
                  sendMessage(message.orderID, 1, adminWS)
                }
                if (qaWS){
                  sendMessage(message.orderID, message.to_qaID, qaWS)
                }
              }
              
              // messageSelect(res.return.insertId, (data)=>{
              //   //console.log(res)
              //   let dat = data.return
              //   //console.log(dat)
              //    friendWS.send(JSON.stringify(dat))
              //    messageUpdate(1, res.return.insertId)
              // })
            }
        }, message.to_xieshouID, message.to_qaID)
        //console.log(result)
        

          // if(friendWS && result.successful===true){
          //   var data = messageSelect(result.insertId)
          //   friendWS.send(data)
          //   messageUpdate(1, result.insertId)
          // }
        
      }  
    }
    // else{
    //   var friendWS = clientMap.get(message.to)
    //   if(friendWS){
    //     //customer online
    //     console.log('client '+message.to +
    //     ' on line, send message from '+ message.from + ' to ' + message.to)
    //     friendWS.send('['+JSON.stringify(message)+']')
    //   }else{
    //     console.log('client '+message.to + ' off line')
    //     //client offline, store message
    //     var hisMessage = messageCacheMap.get(message.to)
    //     if(hisMessage){
    //       hisMessage.push(message)
    //       messageCacheMap.set(message.to, hisMessage)
    //     }else{
    //       messageCacheMap.set(message.to, [message])
    //     }
    //   }
    // }
    // console.log(clientMap)
    // console.log(messageCacheMap)
    // ws.send(message);
    // console.log('received: %s', message);
  });

  // ws.send('connected...');
});
  
module.exports = app;
