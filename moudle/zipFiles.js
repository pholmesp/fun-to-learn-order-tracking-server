let JSZip  = require('jszip')
const fs = require('fs')

const zipFiles = (zipName, fileArray, txtFile, txtContent, path) => {
  let zip = new JSZip()
  let urlArray = fileArray
  if(typeof(urlArray) == 'string') urlArray = JSON.parse(urlArray)
  urlArray = urlArray||[]
  let zipPackage = zip.folder(zipName)
  for(let url of urlArray){
    if(fs.existsSync('data/img/'+url.filename))
      zipPackage.file(url.filename, fs.readFileSync('data/img/'+url.filename))
      // JSZip.readFileSync('data/img/'+url.filename, { encode: null, mimeType: 'text/plain; charset=x-user-defined'});
      // let res =  await fetch( 'http://localhost:3000/api/img/'+url.filename, { encode: null, mimeType: 'text/plain; charset=x-user-defined'});
      // console.log(typeof(res), res)
      // zipPackage.file(url.originalname, res.blob(), {binary:true})
      // saveAs('http://localhost:3000/api/img/'+url.filename, url.filename)
  }
  zipPackage.file(txtFile, txtContent)
  zip.generateAsync({type:"nodebuffer"})
  .then(zipFile=>{
    fs.writeFile(`data/${path}/${zipName}.zip`, zipFile, error=>console.log(error))
  })
}
// zipFiles('test', [], 'test.txt', '', 'assignment')
module.exports = zipFiles
